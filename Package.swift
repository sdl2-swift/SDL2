import PackageDescription

let package = Package(
		name: "SDL2",
		targets: [
			Target(name: "SDL2"),
			Target(name: "SDL2-test", dependencies: ["SDL2"]),
		],
		dependencies: [
			.Package(url: "https://gitlab.com/sdl2-swift/CSdl2.git", majorVersion: 1)
		]
)
