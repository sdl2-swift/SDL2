import CSdl2

public
enum PixelFormat: RawRepresentable {

	public
	typealias RawValue = Int

	case unknown
	case index1lsb
	case index1msb
	case index4lsb
	case index4msb
	case index8
	case rgb332
	case rgb444
	case rgb555
	case bgr555
	case argb4444
	case rgba4444
	case abgr4444
	case bgra4444
	case argb1555
	case rgba5551
	case abgr1555
	case bgra5551
	case rgb565
	case bgr565
	case rgb24
	case bgr24
	case rgb888
	case rgbx8888
	case bgr888
	case bgrx8888
	case argb8888
	case rgba8888
	case abgr8888
	case bgra8888
	case argb2101010
	case rgba32
	case argb32
	case bgra32
	case abgr32
	case yv12
	case iyuv
	case yuy2
	case uyvy
	case yvyu
	case nv12
	case nv21

	public
	init?(rawValue: Int) {
		switch rawValue {
		case SDL_PIXELFORMAT_UNKNOWN:
			self = .unknown
		case SDL_PIXELFORMAT_INDEX1LSB:
			self = .index1lsb
		case SDL_PIXELFORMAT_INDEX1MSB:
			self = .index1msb
		case SDL_PIXELFORMAT_INDEX4LSB:
			self = .index4lsb
		case SDL_PIXELFORMAT_INDEX4MSB:
			self = .index4msb
		case SDL_PIXELFORMAT_INDEX8:
			self = .index8
		case SDL_PIXELFORMAT_RGB332:
			self = .rgb332
		case SDL_PIXELFORMAT_RGB444:
			self = .rgb444
		case SDL_PIXELFORMAT_RGB555:
			self = .rgb555
		case SDL_PIXELFORMAT_BGR555:
			self = .bgr555
		case SDL_PIXELFORMAT_ARGB4444:
			self = .argb4444
		case SDL_PIXELFORMAT_RGBA4444:
			self = .rgba4444
		case SDL_PIXELFORMAT_ABGR4444:
			self = .abgr4444
		case SDL_PIXELFORMAT_BGRA4444:
			self = .bgra4444
		case SDL_PIXELFORMAT_ARGB1555:
			self = .argb1555
		case SDL_PIXELFORMAT_RGBA5551:
			self = .rgba5551
		case SDL_PIXELFORMAT_ABGR1555:
			self = .abgr1555
		case SDL_PIXELFORMAT_BGRA5551:
			self = .bgra5551
		case SDL_PIXELFORMAT_RGB565:
			self = .rgb565
		case SDL_PIXELFORMAT_BGR565:
			self = .bgr565
		case SDL_PIXELFORMAT_RGB24:
			self = .rgb24
		case SDL_PIXELFORMAT_BGR24:
			self = .bgr24
		case SDL_PIXELFORMAT_RGB888:
			self = .rgb888
		case SDL_PIXELFORMAT_RGBX8888:
			self = .rgbx8888
		case SDL_PIXELFORMAT_BGR888:
			self = .bgr888
		case SDL_PIXELFORMAT_BGRX8888:
			self = .bgrx8888
		case SDL_PIXELFORMAT_ARGB8888:
			self = .argb8888
		case SDL_PIXELFORMAT_RGBA8888:
			self = .rgba8888
		case SDL_PIXELFORMAT_ABGR8888:
			self = .abgr8888
		case SDL_PIXELFORMAT_BGRA8888:
			self = .bgra8888
		case SDL_PIXELFORMAT_ARGB2101010:
			self = .argb2101010
		case SDL_PIXELFORMAT_RGBA32:
			self = .rgba32
		case SDL_PIXELFORMAT_ARGB32:
			self = .argb32
		case SDL_PIXELFORMAT_BGRA32:
			self = .bgra32
		case SDL_PIXELFORMAT_ABGR32:
			self = .abgr32
		case SDL_PIXELFORMAT_YV12:
			self = .yv12
		case SDL_PIXELFORMAT_IYUV:
			self = .iyuv
		case SDL_PIXELFORMAT_YUY2:
			self = .yuy2
		case SDL_PIXELFORMAT_UYVY:
			self = .uyvy
		case SDL_PIXELFORMAT_YVYU:
			self = .yvyu
		case SDL_PIXELFORMAT_NV12:
			self = .nv12
		case SDL_PIXELFORMAT_NV21:
			self = .nv21
		default:
			return nil
		}
	}

	public var rawValue: Int {
		switch self {
		case .unknown:
			return SDL_PIXELFORMAT_UNKNOWN
		case .index1lsb:
			return SDL_PIXELFORMAT_INDEX1LSB
		case .index1msb:
			return SDL_PIXELFORMAT_INDEX1MSB
		case .index4lsb:
			return SDL_PIXELFORMAT_INDEX4LSB
		case .index4msb:
			return SDL_PIXELFORMAT_INDEX4MSB
		case .index8:
			return SDL_PIXELFORMAT_INDEX8
		case .rgb332:
			return SDL_PIXELFORMAT_RGB332
		case .rgb444:
			return SDL_PIXELFORMAT_RGB444
		case .rgb555:
			return SDL_PIXELFORMAT_RGB555
		case .bgr555:
			return SDL_PIXELFORMAT_BGR555
		case .argb4444:
			return SDL_PIXELFORMAT_ARGB4444
		case .rgba4444:
			return SDL_PIXELFORMAT_RGBA4444
		case .abgr4444:
			return SDL_PIXELFORMAT_ABGR4444
		case .bgra4444:
			return SDL_PIXELFORMAT_BGRA4444
		case .argb1555:
			return SDL_PIXELFORMAT_ARGB1555
		case .rgba5551:
			return SDL_PIXELFORMAT_RGBA5551
		case .abgr1555:
			return SDL_PIXELFORMAT_ABGR1555
		case .bgra5551:
			return SDL_PIXELFORMAT_BGRA5551
		case .rgb565:
			return SDL_PIXELFORMAT_RGB565
		case .bgr565:
			return SDL_PIXELFORMAT_BGR565
		case .rgb24:
			return SDL_PIXELFORMAT_RGB24
		case .bgr24:
			return SDL_PIXELFORMAT_BGR24
		case .rgb888:
			return SDL_PIXELFORMAT_RGB888
		case .rgbx8888:
			return SDL_PIXELFORMAT_RGBX8888
		case .bgr888:
			return SDL_PIXELFORMAT_BGR888
		case .bgrx8888:
			return SDL_PIXELFORMAT_BGRX8888
		case .argb8888:
			return SDL_PIXELFORMAT_ARGB8888
		case .rgba8888:
			return SDL_PIXELFORMAT_RGBA8888
		case .abgr8888:
			return SDL_PIXELFORMAT_ABGR8888
		case .bgra8888:
			return SDL_PIXELFORMAT_BGRA8888
		case .argb2101010:
			return SDL_PIXELFORMAT_ARGB2101010
		case .rgba32:
			return SDL_PIXELFORMAT_RGBA32
		case .argb32:
			return SDL_PIXELFORMAT_ARGB32
		case .bgra32:
			return SDL_PIXELFORMAT_BGRA32
		case .abgr32:
			return SDL_PIXELFORMAT_ABGR32
		case .yv12:
			return SDL_PIXELFORMAT_YV12
		case .iyuv:
			return SDL_PIXELFORMAT_IYUV
		case .yuy2:
			return SDL_PIXELFORMAT_YUY2
		case .uyvy:
			return SDL_PIXELFORMAT_UYVY
		case .yvyu:
			return SDL_PIXELFORMAT_YVYU
		case .nv12:
			return SDL_PIXELFORMAT_NV12
		case .nv21:
			return SDL_PIXELFORMAT_NV21
		}
	}
}