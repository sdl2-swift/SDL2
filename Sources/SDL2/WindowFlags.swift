import CSdl2

public
struct WindowFlags: OptionSet {

    public
    typealias RawValue = UInt32

    public
    let rawValue: UInt32

    public
    init(rawValue: UInt32) {
        self.rawValue = rawValue
    }

    /// Fullscreen window
    public
    static let fullScreen = WindowFlags(rawValue: SDL_WINDOW_FULLSCREEN.rawValue)
    /// Fullscreen window at the current desktop resolution
    public
    static let fullScreenDesktop = WindowFlags(rawValue: SDL_WINDOW_FULLSCREEN_DESKTOP.rawValue)
    /// Window usable with OpenGL context
    public
    static let openGl = WindowFlags(rawValue: SDL_WINDOW_OPENGL.rawValue)
    /// Window is visible
    public
    static let shown = WindowFlags(rawValue: SDL_WINDOW_SHOWN.rawValue)
    /// Window is not visible
    public
    static let hidden = WindowFlags(rawValue: SDL_WINDOW_HIDDEN.rawValue)
    /// No window decoration
    public
    static let borderless = WindowFlags(rawValue: SDL_WINDOW_BORDERLESS.rawValue)
    /// Window can be resized
    public
    static let resizable = WindowFlags(rawValue: SDL_WINDOW_RESIZABLE.rawValue)
    /// Window is minimized
    public
    static let minimized = WindowFlags(rawValue: SDL_WINDOW_MINIMIZED.rawValue)
    /// Window is maximized
    public
    static let maximized = WindowFlags(rawValue: SDL_WINDOW_MAXIMIZED.rawValue)
    /// Window has grabbed input focus
    public
    static let inputGrabbed = WindowFlags(rawValue: SDL_WINDOW_INPUT_GRABBED.rawValue)
    /// Window has input focus
    public
    static let inputFocus = WindowFlags(rawValue: SDL_WINDOW_INPUT_FOCUS.rawValue)
    /// Window has mouse focus
    public
    static let mouseFocus = WindowFlags(rawValue: SDL_WINDOW_MOUSE_FOCUS.rawValue)
    /// Window not created by SDL
    public
    static let foreign = WindowFlags(rawValue: SDL_WINDOW_FOREIGN.rawValue)
    /// Window should be created in high-DPI mode if supported
    ///
    /// Since: SDL 2.0.1
    public
    static let allowHighDpi = WindowFlags(rawValue: SDL_WINDOW_ALLOW_HIGHDPI.rawValue)
    /// Window has mouse captured (unrelated to inputGrabbed)
    ///
    /// Since: SDL 2.0.4
    public
    static let mouseCapture = WindowFlags(rawValue: SDL_WINDOW_MOUSE_CAPTURE.rawValue)
    /// Window should always be above others (X11 only)
    ///
    /// Since: SDL 2.0.5
    public
    static let alwaysOnTop = WindowFlags(rawValue: SDL_WINDOW_ALWAYS_ON_TOP.rawValue)
    /// Window should not be added to the taskbar (X11 only)
    ///
    /// Since: SDL 2.0.5
    public
    static let skipTaskBar = WindowFlags(rawValue: SDL_WINDOW_SKIP_TASKBAR.rawValue)
    /// Window should be treated as a utility window (X11 only)
    ///
    /// Since: SDL 2.0.5
    public
    static let utility = WindowFlags(rawValue: SDL_WINDOW_UTILITY.rawValue)
    /// Window should be treated as a tooltip (X11 only)
    ///
    /// Since: SDL 2.0.5
    public
    static let tooltip = WindowFlags(rawValue: SDL_WINDOW_TOOLTIP.rawValue)
    /// Window should be treated as a popup menu (X11 only)
    ///
    /// Since: SDL 2.0.5
    public
    static let popupMenu = WindowFlags(rawValue: SDL_WINDOW_POPUP_MENU.rawValue)
}