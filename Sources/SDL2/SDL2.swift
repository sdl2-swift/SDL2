import CSdl2

public struct SubSystem: OptionSet {
	public typealias RawValue = UInt32

	public let rawValue: RawValue

	public static let timer = SubSystem(rawValue: 0x00000001)

	public static let audio = SubSystem(rawValue: 0x00000010)

	public static let video = SubSystem(rawValue: 0x00000020)

	public static let cdrom = SubSystem(rawValue: 0x00000100)

	public static let joystick = SubSystem(rawValue: 0x00000200)

	public static let noParachute = SubSystem(rawValue: 0x00100000)

	public static let eventThread = SubSystem(rawValue: 0x01000000)

	public static let everything = SubSystem(rawValue: 0x0000FFFF)

	public init(rawValue: UInt32) {
		self.rawValue = rawValue
	}
}

public enum SDL2 {
	public static func initialize(_ subSystems: SubSystem) throws {
		if SDL_Init(subSystems.rawValue) != 0 {
			throw SDLError()
		}
	}

	public static func initialize(subSystem: SubSystem) throws {
		if SDL_InitSubSystem(subSystem.rawValue) != 0 {
			throw SDLError()
		}
	}

	public static func quit() {
		SDL_Quit()
	}

	public static func quit(subSystem: SubSystem) {
		SDL_QuitSubSystem(subSystem.rawValue)
	}

	public static func setMainReady() {
		SDL_SetMainReady()
	}

	public static func wasInit() -> SubSystem {
		return SubSystem(rawValue: SDL_WasInit(0))
	}

	public static func wasInit(_ subSystem: SubSystem) -> Bool {
		return SDL_WasInit(subSystem.rawValue) == subSystem.rawValue
	}

	public static func wasInit(_ subSystem: SubSystem) -> SubSystem {
		return SubSystem(rawValue: SDL_WasInit(subSystem.rawValue))
	}

	public static func delay(_ milliseconds: UInt32) {
		SDL_Delay(milliseconds)
	}
}
